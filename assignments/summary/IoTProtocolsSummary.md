# IoT Protocols

## 1. 4-20 mA current loops

- The 4-20 mA current loop has been the standard for signal transmission and electronic control in control systems. In a current loop, the current signal is drawn from a dc power supply, flows through the transmitter, into the controller and then back to the power supply in a series circuit.
- The advantage is that the current value does not degrade over long distances, so the current signal remains constant through all components in the loop.
- As a result, the accuracy of the signal is not affected by a voltage drop in the interconnecting wiring.

![4-20 mA](assignments/extras/4-20_mA.jpg)

| Pros: |
|------|
| The 4-20 mA current loop is the dominant standard in many industries. | 
| It is the simplest option to connect and configure. | 
| It uses less wiring and connections than other signals, greatly reducing initial setup costs. | 
| Better for traveling long distances, as current does not degrade over long connections like voltage. | 
| It is less sensitive to background electrical noise. | 
| Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system. | 

| Cons: |
|------|
| Current loops can only transmit one particular process signal. |
| Multiple loops must be created in situations where there are numerous process variables that require transmission. |
| Running so much wire could lead to problems with ground loops if independent loops are not properly isolated. |
| These isolation requirements become exponentially more complicated as the number of loops increases. |

<b>Overview</b>

- The 4-20 mA current loop is the prevailing process control signal in many industries.
- It is an ideal method of transferring process information because current does not change as it travels from transmitter to receiver. 
- It is also much simpler and cost effective. 
- However, voltage drops and the number of process variables that need to be monitored can impact its cost and complexity.

## 2. MODBUS

- Modbus is a serial communication protocol, especially for use with programmable logic controllers (PLC). 
- It is typically used to transmit signals from instrumentation and control devices back to a main controller, or data gathering system.
- In simple terms, it is a method used for transmitting information over serial lines between electronic devices. 
- The device requesting the information is called the Modbus Master and the devices supplying information are Modbus Slaves. 
- he protocol is commonly used in IoT as a local interface to manage devices.

![Modbus](assignments/extras/Modbus.png)

### What is it used for?

- Modbus is typically used to transmit signals from instrumentation and control devices back to a main controller or data gathering system, for example a system that measures temperature and humidity and communicates the results to a computer. 
- Modbus is often used to connect a supervisory computer with a remote terminal unit (RTU) in supervisory control and data acquisition (SCADA) systems. 
- Modbus protocol can be used over 2 interfaces: 1) RS485 - called as Modbus RTU, 2) Ethernet - called as Modbus TCP/IP

### How MODBUS protocol works?

- Modbus is transmitted over serial lines between devices. 
- The simplest setup would be a single serial cable connecting the serial ports on two devices, a Master and a Slave. 
- The data is sent as series of ones and zeroes called bits. 
- Each bit is sent as a voltage.
- Zeroes are sent as positive voltages and a ones as negative. 
- The bits are sent very quickly. 
- A typical transmission speed is 9600 baud (bits per second).

![Modbus work](assignments/extras/Modbus_work.PNG)

### RS485

- RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
- RS485 is not directly compatible: you must use the correct type of interface, or the signals won't go through. 
- Mainly done through an easy to use an RS485 to USB.
- RS485 was developed to provide high speed data. 
- The standard is defined by industry telecommunications bodies and may be referred to most commonly as RS485.
- RS485 is able to provide a headline data rate of 10 Mbps at distances up to 50 feet, but distances can be extended to 4000 feet with a lower speed of 100 kbps.
- Although RS485 was never intended for domestic use, it found many applications where remote data acquisition was required.

![RS485](assignments/extras/RS485.jpg)

## 3. OPC UA

OPC Unified Architecture (OPC UA) is a machine to machine communication protocol for industrial automation developed by the OPC Foundation.

Distinguishing characteristics are:

- Focus on communicating with industrial equipment and systems for data collection and control.
- Open - freely available and implementable under GPL 2.0 license.
- Cross-platform - not tied to one operating system or programming language.
- Service-oriented architecture (SOA)
- Offers security functionality for authentication, authorization, integrity and confidentiality.
- Key industries include pharmaceutical, oil and gas, building automation, industrial robotics, security, manufacturing and process control.

![OPC UA](assignments/extras/OPCUA.jpg)

# Cloud Protocols

## 1. MQTT

- MQTT (MQ Telemetry Transport) is a lightweight messaging protocol that provides resource-constrained network clients with a simple way to distribute telemetry information. 
- The protocol, which uses a publish/subscribe communication pattern, is used for machine-to-machine (M2M) communication and plays an important role in the internet of things (IoT).
- The MQTT protocol is a good choice for wireless networks that experience varying levels of latency due to occasional bandwidth constraints or unreliable connections.
- The MQTT protocol surrounds two subjects: a client and a broker.
- An MQTT broker is a server, while the clients are the connected devices. 
- When a device or client wants to send data to a server or broker it is called a publish. 
- When the operation is reversed, it is called a subscribe.

### Working 

- MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.
- Topics are a way to register interest for incoming messages or to specify where to publish the message. 
- Represented by strings, separated by forward slash. 
- Each slash indicates a topic level.

![MQTT work](assignments/extras/MQTT_work.jpg)

## 2. HTTP

- The Hypertext Transfer Protocol (HTTP) is an application protocol for distributed, collaborative, hypermedia information systems. 
- HTTP is the foundation of data communication for the World Wide Web, where hypertext documents include hyperlinks to other resources that the user can easily access. 
- For example by a mouse click or by tapping the screen in a web browser.

### Working 

- As a request-response protocol, HTTP gives users a way to interact with web resources such as HTML files by transmitting hypertext messages between clients and servers.
- HTTP clients generally use Transmission Control Protocol (TCP) connections to communicate with servers.

![HTTP work](assignments/extras/HTTP.png)

### The Request

<b>Request has 3 parts:</b>

- Request line
- HTTP headrs
- message body

**GET request**
it is a type of HTTP request using the GET method.

<b>HTTP utilizes specific request methods in order to perform various tasks:</b>

- GET requests a specific resource in its entirety.
- HEAD requests a specific resource without the body content.
- POST adds content, messages, or data to a new page under an existing web resource.
- PUT directly modifies an existing web resource or creates a new URL if needed.
- DELETE gets rid of a specified resource.
- TRACE shows users any changes or additions made to a web resource.
- OPTIONS shows users which HTTP methods are available for a specific URL.
- CONNECT converts the request connection to a transparent TCP/IP tunnel.
- PATCH partially modifies a web resource.

### The Response

<b>Response is made of 3 parts:</b>

- Status line
- HTTP header
- Message body

#### HTTP response status codes:

![Response](assignments/extras/Response.PNG)


## <b> CoAP - Constrained Application Protocol</b>

- Constrained Application Protocol (CoAP) is a specialized web transfer protocol for use with constrained nodes and constrained networks in the Internet of Things. 
- CoAP is designed to enable simple, constrained devices to join the IoT even through constrained networks with low bandwidth and low availability. 
- It is generally used for machine-to-machine (M2M) applications such as smart energy and building automation. 

### Working

- CoAP functions as a sort of HTTP for restricted devices, enabling equipment such as sensors or actuators to communicate on the IoT
- These sensors and actuators are controlled and contribute by passing along their data as part of a system.
- COAP uses UDP as the underlying network protocol. COAP is basically a client-server IoT protocol where the client makes a request and the server     sends back a response as it happens in HTTP. The methods used by COAP are the same used by HTTP

![coAP](https://data-flair.training/blogs/wp-content/uploads/sites/2/2018/06/IOT-PROTOCOLS-IMAGE-1.jpg)
