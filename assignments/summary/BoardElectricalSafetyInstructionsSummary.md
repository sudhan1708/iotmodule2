 Board Electrical Safety Instructions

## Electrical Safety

Electrical safety is a system of organizational measures and technical means to prevent harmful and dangerous effects on workers from electric current, electric arc, electromagnetic field and static electricity.

<b> Since due to current situation, most of the members are doing their work from their home. So, lets know about the electrical safety during this work from home by below picture: </b>

![Electrical Safety](assignments/extras/electrical_safety.PNG)

### 1. Power Supply:

<b>Do’s:</b>

- Always make sure that the output voltage of the power supply matches the input voltage of the board.
- While turning on the power supply make sure every circuit is connected properly.

<b>Don’ts:</b>

- Do not connect power supply without matching the power rating.
- Never connect a higher output (12V/3Amp) to a lower (5V/2Amp) input.
- Do not try to force the connector into the power socket ,it may damage the connector.

![Power supply](assignments/extras/Power_supply.jpg)

### 2. Handling:

<b>Do’s:</b>

- Treat every device like it is energized, even if it does not look like it is plugged in or operational.
- While working keep the board on a flat stable surface (wooden table) .
- Unplug the device before performing any operation on them.
- When handling electrical equipment, make sure your hands are dry.
- Keep all electrical circuit contact points enclosed.
- If the board becomes too hot try to cool it with a external usb fan.

<b>Don’ts:</b>

- Don’t handle the board when its powered ON.
- Never touch electrical equipment when any part of your body is wet, (that includes fair amounts of perspiration).
- Do not touch any sort of metal to the development board.


### 3. GPIO:

![GPIO](assignments/extras/GPIO.jpg)

<b>Do’s:</b>

- Find out whether the board runs on 3.3v or 5v logic.
- Always connect the LED (or sensors) using appropriate resistors.
- To Use 5V peripherals with 3.3V we require a logic level converter.

<b>Don’ts:</b>

- Never connect anything greater that 5v to a 3.3v pin.
- Avoid making connections when the board is running.
- Don't plug anything with a high (or negative) voltage.
- Do not connect a motor directly , use a transistor to drive it.

## Guidelines for using interfaces

### 1. UART

A universal asynchronous receiver/transmitter (UART) is a block of circuitry responsible for implementing serial communication. Essentially, the UART acts as an intermediary between parallel and serial interfaces.

![UART](assignments/extras/UART.PNG)

- Connect Rx pin of device1 to Tx pin of device2, similarly Tx pin of device1 to Rx pin of device2.
- If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism (voltage divider).
- Generally, UART is used to communicate with board through USB to TTL connection.
- USB to TTL connection does not require a protection circuit.
- Whereas Senor interfacing using UART might require a protection circuit.

### 2. I2C

I2C combines the best features of SPI and UARTs. With I2C, you can connect multiple slaves to a single master (like SPI) and you can have multiple masters controlling single, or multiple slaves. This is really useful when you want to have more than one microcontroller logging data to a single memory card or displaying text to a single LCD.

![I2C](assignments/extras/I2C.jpg)

- While using I2C interfaces with sensors SDA and SDL lines must be protected.
- Protection of these lines is done by using pullup registers on both lines.
- If you use the inbuilt pullup registers in the board you won't need an external circuit.
- If you are using bread-board to connect your sensor, use the pullup resistor.
- Generally, resistors of value between 2.2k ohm and 4k ohm are used.

### 3. SPI

The Serial Peripheral Interface (SPI) is a synchronous serial communication interface specification used for short-distance communication, primarily in embedded systems. Typical applications include Secure Digital cards and liquid crystal displays.

![SPI](assignments/extras/SPI.png)

- Generally, SPI in development boards is in Push-pull mode.
- Push-pull mode does not require any protection circuit.
- On SPI interface, if you are using more than one slaves it is possible that the device2 can "hear" and "respond" to the master's communication with device1- which is an disturbance.
- To overcome this problem, we use a protection circuit with pullup resistors on each the Slave Select line.
- Resistors value can be between 1k ohm and 10k ohm.
- Generally, 4.7k ohm resistor is used.